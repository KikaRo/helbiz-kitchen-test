package helbizkitchen_test;

import helbizkitchen.page_objects.DashboardPage;
import helbizkitchen.page_objects.LoginPage;
import helbizkitchen.webdriver.BrowserDriver;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.*;
import org.testng.asserts.SoftAssert;

import static org.hamcrest.MatcherAssert.assertThat;

public class LoginTest {

    WebDriver driver;
    DashboardPage dashboardPage;
    LoginPage loginPage;

    @BeforeMethod
    public void setUp() {
        driver = BrowserDriver.getDriver();
        dashboardPage = new DashboardPage(driver);
        assertThat("Dashboard page wasn't opened successfully.", dashboardPage.isActive());

        loginPage = dashboardPage.clickOnGetStartedButton();
        assertThat("Login page was not opened successfully.", loginPage.isActive());
    }


    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) {
        TestBase.takeScreenshot(result,driver);
        BrowserDriver.close();
    }

    @Test
    public void loginEmptyEmailInputTest() {
        if(!loginPage.isEmailInputEmpty())
            loginPage.clearEmailInput();
        loginPage.fillPasswordInput(loginPage.getTestConfig().getPassword());

        SoftAssert soft = new SoftAssert();
        soft.assertTrue(loginPage.isEmailInputErrorVisible());
        soft.assertFalse(loginPage.isLoginButtonClickable());
        soft.assertAll();
    }

    @Test
    public void loginInvalidPasswordTest() {
        loginPage.fillEmailInput("kikaro98@gmail.com");
        loginPage.fillPasswordInput("Bad password");
        assertThat("Login button isn't clickable even though input fields are not empty.", loginPage.isLoginButtonClickable());

        loginPage.clickOnLoginButton();

        SoftAssert soft = new SoftAssert();
        soft.assertTrue(loginPage.isBadPasswordErrorMessageVisible(),"The message that the wrong password was entered is not displayed.");
        soft.assertTrue(loginPage.isActive(),"The login page is inactive after a failed login to the system.");
        soft.assertAll();
    }

    @Test
    public void loginSuccessTest() {
        loginPage.fillEmailInput("helbiztesting@gmail.com");
        loginPage.fillPasswordInput("helbiztesting1");
        assertThat("Login button isn't clickable even though input fields are not empty.", loginPage.isLoginButtonClickable());

        loginPage.clickOnLoginButton();

        SoftAssert soft = new SoftAssert();
        soft.assertFalse(loginPage.isActive(), "Login page is still active after successful login");
        soft.assertTrue(dashboardPage.isAccountIdentifierVisible());
        soft.assertAll();
    }
}
