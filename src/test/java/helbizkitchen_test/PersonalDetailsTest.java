package helbizkitchen_test;

import helbizkitchen.page_objects.AccountSideBarNavigationPage;
import helbizkitchen.page_objects.DashboardPage;
import helbizkitchen.page_objects.LoginPage;
import helbizkitchen.page_objects.PersonalDetailsPage;
import helbizkitchen.webdriver.BrowserDriver;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.*;
import org.testng.asserts.SoftAssert;

import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;


import static org.hamcrest.MatcherAssert.assertThat;

public class PersonalDetailsTest {

    WebDriver driver;
    static DashboardPage dashboardPage;
    AccountSideBarNavigationPage accountSideBarNavigationPage;
    LoginPage loginPage;
    PersonalDetailsPage personalDetailsPage;


    static String email = "helbiztesting@gmail.com";
    static String password = "helbiztesting1";
    static String fn;
    static String ln;

    public String setFirstName() {
       fn =  "FirstName Updated" + RandomStringUtils.randomNumeric(3,3);
       return fn;
    }

    public String setLastName() {
        ln =  "LastName Updated" + RandomStringUtils.randomNumeric(3,3);
        return ln;
    }

    @DataProvider(name = "updatingPersonalInformation")
    private Object[][] udatingPersonalInformation() {
        return new Object[][]{
                {true,  true },
                {true,  false},
                {false, true }};
    }

    @BeforeClass
    public void setUp() {
        driver = BrowserDriver.getDriver();
        dashboardPage = new DashboardPage(driver);
        assertThat("Dashboard page wasn't opened successfully.", dashboardPage.isActive());

        loginPage = dashboardPage.clickOnGetStartedButton();
        loginPage.loginUser(email, password);
        assertThat("User was not logged in successfuly.", dashboardPage.isActive());

        accountSideBarNavigationPage = dashboardPage.openAccountSideBar();
        accountSideBarNavigationPage.wait(20);

        personalDetailsPage = accountSideBarNavigationPage.openPersonalDetailsPage();
        Assert.assertTrue(personalDetailsPage.isActive());
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() { BrowserDriver.close(); }

    @Test
    public void uploadPhotoSuccessTest() throws AWTException {
        personalDetailsPage.clickOnUploadPhoto();
        StringSelection ss = new StringSelection("C:\\Users\\Lenovo\\Desktop\\helbiz1");
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
        Robot robot = new Robot();
        robot.delay(250);
        robot.keyPress(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_ENTER);
        robot.delay(250);
        robot.keyRelease(KeyEvent.VK_ENTER);

        Assert.assertTrue(personalDetailsPage.isProfileUpdatedMessageVisible(),"Message that the photo is successfully updated didn't show");
    }

    @Test(dataProvider = "updatingPersonalInformation")
    public void saveChangesOnPersonalDetailsPageTest(boolean firstName, boolean lastName) {
        if(firstName)
            personalDetailsPage.changeFirstName(setFirstName());
        if(lastName)
            personalDetailsPage.changeLastName(setLastName());

        Assert.assertTrue(personalDetailsPage.isSubmitButtonClickable(), "Submit button isn't clickable");
        personalDetailsPage.clickOnSubmitButton();

        SoftAssert soft = new SoftAssert();
        soft.assertTrue(personalDetailsPage.isProfileUpdatedMessageVisible(), "Message for successful update didn't show.");
        if(firstName)
            soft.assertEquals(personalDetailsPage.getFirstName(), fn);

        if(lastName)
            soft.assertEquals(personalDetailsPage.getLastName(), ln);

        soft.assertFalse(personalDetailsPage.isSubmitButtonClickable());
        soft.assertAll();
    }
}
