package helbizkitchen_test;

import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;

import helbizkitchen.utils.ScreenshotUtil;

public class TestBase {

        public static void takeScreenshot(ITestResult result, WebDriver driver) {
            if(result.getStatus() == ITestResult.FAILURE) {
                ScreenshotUtil.makeScreenshot(driver, result.getMethod().getMethodName());
            }
        }
}
