package helbizkitchen_test.signUp;

import helbizkitchen.page_objects.DashboardPage;
import helbizkitchen.page_objects.LoginPage;
import helbizkitchen.page_objects.MenuDashboardPage;
import helbizkitchen.webdriver.BrowserDriver;
import helbizkitchen_test.TestBase;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;

public class OrderItemsWhenLoggedInTest {

    WebDriver driver;
    DashboardPage dashboardPage;
    LoginPage loginPage;
    MenuDashboardPage menuDashboardPage;


    @BeforeClass
    public void setUp() {
        driver = BrowserDriver.getDriver();
        dashboardPage = new DashboardPage(driver);
        assertThat("Dashboard page wasn't opened successfully.", dashboardPage.isActive());

        loginPage = dashboardPage.clickOnGetStartedButton();
        assertThat("Login page was not opened successfully.", loginPage.isActive());
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result) {
        TestBase.takeScreenshot(result, driver);
        BrowserDriver.close();
    }

    @Test
    public void orderItemsWhenLoggedInTest() {

    }
}
