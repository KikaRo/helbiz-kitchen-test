package helbizkitchen_test.signUp;

import helbizkitchen.page_objects.DashboardPage;
import helbizkitchen.page_objects.LoginPage;
import helbizkitchen.page_objects.SignUpPage;
import helbizkitchen.webdriver.BrowserDriver;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;

public class SignUpTest {

    WebDriver driver;
    DashboardPage dashboardPage;
    LoginPage loginPage;
    SignUpPage signUpPage;

    @DataProvider(name = "invalidEmailInput")
    private Object[][] getInvalidEmailInput() {
        return new Object[][]{
                {"example.com",                   "password1" },
                {"e@xample@gmail.com",            "password1" },
                {"a”b(c)d,e:f;gi[jk]l@gmail.com", "password1" },
                {"abc is”not valid@gmail.com",    "password1" },
                {".test@gmail.com",               "password1" },
                {"test@gmail..com",               "password1" },
                {" kikaro98@gmail.com",           "password1" },
                {"kikaro98@gmail.com ",           "password1" }};
    }

    @DataProvider(name = "invalidPasswordInput")
    private Object[][] getInvalidPasswordInput() {
        return new Object[][]{
                {"test@gmail.com",  "a"     },
                {"test@gmail.com",  "aa"    },
                {"test@gmail.com",  "aaa"   },
                {"test@gmail.com",  "aaaa"  },
                {"test@gmail.com",  "aaaaa" },
                {"test@gmail.com",  "aaaaaa" },
                {"test@gmail.com",  "123456" }};
    }

    @BeforeClass
    public void setUp() {
        driver = BrowserDriver.getDriver();
        dashboardPage = new DashboardPage(driver);
        assertThat("Dashboard page wasn't opened successfully.", dashboardPage.isActive());
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() { BrowserDriver.close(); }

    @Test(priority = 0)
    public void getStartedButtonTest() {
        Assert.assertTrue(dashboardPage.isGetStartedButtonClickable(), "Get Started button is not clickable.");
    }

    @Test(priority = 1)
    public void openSignUpScreenTest() {
        loginPage = dashboardPage.clickOnGetStartedButton();
        Assert.assertTrue(loginPage.isActive(), "Login page was not successfully opened.");

        signUpPage = loginPage.clickOnSignUpButton();
        Assert.assertTrue(signUpPage.isActive(),"Sign up page was not successfully opened.");
    }

    @Test(dataProvider = "invalidEmailInput", priority = 2)
    public void signUpWithInvalidEmailFormatTest(String invalidEmail, String validPassword) {
        signUpPage.fillEmailInput(invalidEmail);
        signUpPage.fillPasswordInput(validPassword);
        signUpPage.fillConfirmPasswordInput(validPassword);
        Assert.assertFalse(signUpPage.isNextButtonClickable(), "Next button is clickable even the entered email adress is invalid.");
    }

    @Test(dataProvider = "invalidPasswordInput", priority = 3)
    public void signUpWithInvalidPasswordTest(String validEmail, String invalidPassword) {
        signUpPage.fillEmailInput(validEmail);
        signUpPage.fillPasswordInput(invalidPassword);
        signUpPage.fillConfirmPasswordInput(invalidPassword);
        Assert.assertFalse(signUpPage.isNextButtonClickable(), "Next button is clickable even entered password is invalid.");
    }

    @Test(priority = 4)
    public void signUpWithDifferentPasswordAndConfirmationPasswordTest() {
        signUpPage.fillEmailInput("test@gmail.com");
        signUpPage.fillPasswordInput("password1");
        signUpPage.fillConfirmPasswordInput("password2");
        Assert.assertFalse(signUpPage.isNextButtonClickable(),"Next button is clickable even password and confirm password are not equal.");
    }

    @Test(priority = 5)
    public void decryptedPasswordTest() {
        if(signUpPage.isPasswordEncrypted())
            signUpPage.clickOnVisibilityButtonPassword();
        Assert.assertFalse(signUpPage.isPasswordEncrypted());
    }


}
