package helbizkitchen_test.signUp;

import helbizkitchen.page_objects.DashboardPage;
import helbizkitchen.page_objects.LoginPage;
import helbizkitchen.page_objects.MenuDashboardPage;
import helbizkitchen.page_objects.SignUpPage;
import helbizkitchen.webdriver.BrowserDriver;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;

public class OrderItemsWhenNotLoggedInTest {

    WebDriver driver;
    DashboardPage dashboardPage;
    LoginPage loginPage;
    MenuDashboardPage menuDashboardPage;

    @BeforeClass
    public void setUp() {
        driver = BrowserDriver.getDriver();
        dashboardPage = new DashboardPage(driver);
        assertThat("Dashboard page wasn't opened successfully.", dashboardPage.isActive());
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() { BrowserDriver.close(); }

    @Test(priority=6)
    public void orderItemsWithoutLoggedInTest() {
       menuDashboardPage = dashboardPage.clickOnExploreKitchenButton();
       Assert.assertTrue(menuDashboardPage.isActive(),"Menu Page did not open successfully.");

       menuDashboardPage.clickOnPizzaMenu();
       menuDashboardPage.clickOnElement(menuDashboardPage.findFirstElementThatContainsText("Schiacciatina Rossa"));
       Assert.assertTrue(menuDashboardPage.isMenuItemPopupActive(),"Menu item popup did not open successfully.");

       loginPage = menuDashboardPage.clickOnAddToOrderButtonWithoutLogingIn();
       Assert.assertTrue(loginPage.isActive(),"Login Page did not open after trying to add an order without loging in.");
    }
}
