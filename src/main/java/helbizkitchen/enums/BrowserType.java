package finplan.enums;

public enum BrowserType {
    FIREFOX, CHROME, CHROME_HEADLESS, CHROME_DOCKER, EDGE
}