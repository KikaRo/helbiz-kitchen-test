package helbizkitchen.page_objects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class PersonalDetailsPage extends BasePage {

    @FindBy(xpath = "//div[@class='personal-details ']")
    private WebElement pageIdentifier;

    @FindBy(xpath = "//div[@class='personal-details__media__avatar']")
    private WebElement iconPlaceHolder;

    @FindBy(xpath = "//div[@class='snackbar__content']")
    private WebElement popupMessage;

    @FindBy(xpath = "//div[@class='personal-details__data']//input[@name='firstName']")
    private WebElement firstNameInput;

    @FindBy(xpath = "//div[@class='personal-details__data']//input[@name='lastName']")
    private WebElement lastNameInput;

    @FindBy(xpath = "//button[@type='submit']")
    private WebElement submitButton;

    public PersonalDetailsPage(WebDriver driver) { super(driver); }

    public boolean isActive() { return isElementVisible(pageIdentifier); }

    public void clickOnUploadPhoto() { clickOnElement(iconPlaceHolder); }

    public boolean isProfileUpdatedMessageVisible() {
        return isElementVisible(popupMessage,5) && popupMessage.getText().contains("Profile successfully updated");
    }

    public void changeFirstName(String firstName) {
        firstNameInput.clear();
        fillInInputField(firstNameInput, firstName);
    }

    public void changeLastName(String lastName) {
        lastNameInput.clear();
        fillInInputField(lastNameInput, lastName);
    }

    public boolean isSubmitButtonClickable() { return isElementClickable(submitButton); }

    public void clickOnSubmitButton() { clickOnElement(submitButton); }

    public String getFirstName() {  return getElementAttributeValue(firstNameInput,"value"); }

    public String getLastName() { return getElementAttributeValue(lastNameInput,"value"); }

}
