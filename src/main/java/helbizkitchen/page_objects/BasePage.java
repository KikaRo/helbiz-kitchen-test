package helbizkitchen.page_objects;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.*;
import org.openqa.selenium.TimeoutException;

import helbizkitchen.config.TestConfig;
import helbizkitchen.utils.TimeUtil;


public class BasePage {

    protected WebDriver driver = null;
    protected TestConfig testConfig;

    public BasePage(WebDriver driver) {
        this.driver = driver;
        this.testConfig = new TestConfig();
        PageFactory.initElements(driver, this);
    }

    public TestConfig getTestConfig() {
        return testConfig;
    }

    public void wait(float seconds) {
        try {
            Thread.sleep((long) (seconds * 1000));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void wait(int seconds) {
        wait((float) seconds);
    }

    // Element Visibility

    public Boolean isElementVisible(WebElement element, int timeout) throws TimeoutException {
        WebDriverWait wait = new WebDriverWait(driver, timeout);
        try {
            wait.until(ExpectedConditions.visibilityOf(element));
        } catch (TimeoutException e) {
            return false;
        }
        return true;
    }

    public Boolean isElementVisible(WebElement element) {
        return isElementVisible(element, TimeUtil.ELEMENT_VISIBLE_TIME);
    }

    public void waitUntilElementIsVisible(WebElement element, int timeout) {
        WebDriverWait wait = new WebDriverWait(driver, timeout);
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    public void waitUntilElementIsVisible(WebElement element) {
        WebDriverWait wait = new WebDriverWait(driver, TimeUtil.ELEMENT_VISIBLE_TIME);
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    // Element Invisability

    private static ExpectedCondition<Boolean> invisibilityOf(final WebElement element) {
        return new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver webDriver) {
                try {
                    webDriver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
                    return !element.isDisplayed();
                } catch (Exception e) {
                    return true;
                } finally {
                    webDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
                }
            }

            public String toString() {
                return "invisibility of " + element;
            }
        };
    }

    public Boolean isElementInvisible(WebElement element, int timeout) throws TimeoutException {

        WebDriverWait wait = new WebDriverWait(driver, timeout);
        try {
            wait.until(invisibilityOf(element));
        } catch (TimeoutException e) {
            return false;
        }
        return true;
    }

    public Boolean isElementInvisible(WebElement element) {
        return isElementInvisible(element, TimeUtil.ELEMENT_DISAPPEAR_TIME);
    }

    // Click on element
    public void waitUntilElementIsClickable(WebElement element, int timeout) {
        WebDriverWait wait = new WebDriverWait(driver, timeout);
        wait.until(ExpectedConditions.visibilityOf(element));
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    public void clickOnElement(WebElement element) {
        waitUntilElementIsClickable(element, TimeUtil.ELEMENT_VISIBLE_TIME);
        element.click();
    }


    public boolean isElementClickable(WebElement element) {
        WebDriverWait wait = new WebDriverWait(driver, 5);
        try {
            wait.until(ExpectedConditions.elementToBeClickable(element));
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public void clickOnNonClickableElement(WebElement element) {
        waitUntilElementIsVisible(element, TimeUtil.ELEMENT_VISIBLE_TIME);
        element.click();
    }

    // Input fields

    public void pressEnterOnElement(WebElement element) {
        waitUntilElementIsVisible(element);
        element.sendKeys(Keys.RETURN);
    }

    public void fillInInputField(WebElement element, String value, int timeout) {
        waitUntilElementIsVisible(element, timeout);
        //element.clear();
        //this is workaround for clear function, should be fixed
        element.sendKeys(Keys.CONTROL, "aa", Keys.DELETE);
        element.sendKeys(value);
    }

    public void fillInInputField(WebElement element, String value) {
        waitUntilElementIsVisible(element, TimeUtil.ELEMENT_VISIBLE_TIME);
        element.sendKeys(Keys.CONTROL, "aa", Keys.DELETE);
        element.sendKeys(value);
    }

    public void fillInInputFieldAndPressEnter(WebElement element, String value) {
        waitUntilElementIsVisible(element, TimeUtil.ELEMENT_VISIBLE_TIME);
        element.clear();
        element.sendKeys(value);
        element.sendKeys(Keys.RETURN);
    }

    public String getElementText(WebElement element) {
        try {
            waitUntilElementIsVisible(element);
            return element.getText();
        } catch (NoSuchElementException e) {
            return null;
        }
    }

    // Attributes

    public boolean isAttributePresent(WebElement element, String attribute) {
        return isElementVisible(element) && (element.getAttribute(attribute) != null);
    }

    public String getElementAttributeValue(WebElement element, String attribute) {
        if (isAttributePresent(element, attribute)) {
            return element.getAttribute(attribute);
        }
        return null;
    }

    // Displayed elements

    public List<WebElement> findDisplayedElements(By locator) {
        List<WebElement> allElements = driver.findElements(locator);
        List<WebElement> displayedElements = new ArrayList<WebElement>();
        for (WebElement element : allElements) {
            if (element.isDisplayed()) {
                displayedElements.add(element);
            }
        }
        return displayedElements;
    }

    public List<WebElement> findDisplayedElements(WebElement rootElement, By locator) {
        List<WebElement> allElements = rootElement.findElements(locator);
        List<WebElement> displayedElements = new ArrayList<WebElement>();
        for (WebElement element : allElements) {
            if (element.isDisplayed()) {
                displayedElements.add(element);
            }
        }
        return displayedElements;
    }

    // Elements with matching text

    public List<WebElement> findElementsWithMatchingText(String text) {
        String xpathExpression = "//*[contains(text(), '" + text + "')]";
        List<WebElement> allElements = findDisplayedElements(By.xpath(xpathExpression));
        List<WebElement> matchingElements = new ArrayList<WebElement>();
        for (WebElement element : allElements) {
            String elementText = getElementText(element);
            if (text.equalsIgnoreCase(elementText)) {
                matchingElements.add(element);
            }
        }
        return matchingElements;
    }

    public WebElement findFirstElementWithMatchingText(String text) {
        List<WebElement> matchingElements = findElementsWithMatchingText(text);
        if (!matchingElements.isEmpty()) {
            return matchingElements.get(0);
        } else {
            return null;
        }
    }


    public List<WebElement> findElementsThatContainsText(String text) {
        String xpathExpression = "//*[contains(text(), '" + text + "')]";
        List<WebElement> allElements = findDisplayedElements(By.xpath(xpathExpression));
        List<WebElement> matchingElements = new ArrayList<WebElement>();
        for (WebElement element : allElements) {
            String elementText = getElementText(element);
            if (elementText.contains(text)) {
                matchingElements.add(element);
            }
        }
        return matchingElements;
    }

    public WebElement findFirstElementThatContainsText(String text) {
        List<WebElement> matchingElements = findElementsThatContainsText(text);
        if (!matchingElements.isEmpty()) {
            return matchingElements.get(0);
        } else {
            return null;
        }
    }

    public List<WebElement> findElementsThatContainsText(WebElement rootElement, String text) {
        String xpathExpression = ".//*[contains(text(), '" + text + "')]";
        List<WebElement> allElements = findDisplayedElements(rootElement, By.xpath(xpathExpression));
        List<WebElement> matchingElements = new ArrayList<WebElement>();
        for (WebElement element : allElements) {
            String elementText = getElementText(element);
            if (elementText.contains(text)) {
                matchingElements.add(element);
            }
        }
        return matchingElements;
    }

}
