package helbizkitchen.page_objects;

import org.codehaus.plexus.util.dag.DAG;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends BasePage {

    @FindBy(xpath = "//div[@class='login']")
    private WebElement pageIdentifier;

    @FindBy(xpath = "//button[@type='submit']")
    private WebElement loginButton;

    @FindBy(xpath = "//input[@name='email']")
    private WebElement emailInput;

    @FindBy(xpath = "//input[@name='password']")
    private WebElement passwordInput;

    @FindBy(xpath = "//p[@class='login__registration-message']//a[@href='/register']")
    private WebElement signUpButton;

    @FindBy(xpath = "//input[@class='input-error-highlight']")
    private WebElement emailInputError;

    @FindBy(xpath = "//input[@class='input-error-highlight input-password']")
    private WebElement passwordInputError;

    public LoginPage(WebDriver driver) { super(driver); }

    public boolean isActive() {
        return isElementVisible(pageIdentifier) &&
                pageIdentifier.getText().contains("Create an account or log in");
    }

    public void clickOnLoginButton() { clickOnElement(loginButton); }

    public void fillEmailInput(String email) { fillInInputField(emailInput,email); }

    public void fillPasswordInput(String password) { fillInInputField(passwordInput,password); }

    public SignUpPage clickOnSignUpButton() {
        clickOnElement(signUpButton);
        return new SignUpPage(driver);
    }

    public boolean isLoginButtonClickable() { return isElementClickable(loginButton); }

    public boolean isEmailInputEmpty() {
        return emailInput.getText().isEmpty();
    }

    public void clearEmailInput() { emailInput.clear(); }

    public boolean isEmailInputErrorVisible() { return isElementVisible(emailInputError); }

    public boolean isPasswordInputErrorVisible() { return isElementVisible(passwordInputError); }

    public boolean isBadPasswordErrorMessageVisible() { return isElementVisible(findFirstElementThatContainsText("Bad password"),5); }

    public void loginUser(String email, String password) {
        fillEmailInput(email);
        fillPasswordInput(password);
        clickOnElement(loginButton);
    }
}
