package helbizkitchen.page_objects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.awt.*;

public class DashboardPage extends BasePage {

    @FindBy(xpath = "//div[@class='landing']//h1[@class='landing__title']")
    private WebElement pageIdentifier;

    @FindBy(xpath = "//div[@class='app-bar ']")
    private WebElement pageHeader;

    @FindBy(xpath = "//div[@class='app-bar ']//button[@type='button']")
    private WebElement getStartedButton;

    @FindBy(xpath = "//img[@alt='me']")
    private WebElement accountIdentifier;

    @FindBy(xpath = "//button[@class='button button--standard landing__action__explore-button button-focus-visible']")
    private WebElement exploreKitchenButton;

    public DashboardPage(WebDriver driver) {
        super(driver);
        driver.navigate().to(testConfig.getUrl());
    }

    public boolean isActive() {
        return isElementVisible(pageIdentifier) &&
                pageIdentifier.getText().contains("A new era") &&
                pageIdentifier.getText().contains("of food delivery");
    }

    public boolean isGetStartedButtonVisible() { return isElementVisible(getStartedButton); }

    public boolean isGetStartedButtonClickable() { return isElementClickable(getStartedButton); }

    public LoginPage clickOnGetStartedButton() {
        clickOnElement(getStartedButton);
        return new LoginPage(driver);
    }

    public boolean isAccountIdentifierVisible() { return isElementVisible(accountIdentifier); }

    public AccountSideBarNavigationPage openAccountSideBar() {
        clickOnElement(accountIdentifier);
        return new AccountSideBarNavigationPage(driver);
    }

    public MenuDashboardPage clickOnExploreKitchenButton() {
        clickOnElement(exploreKitchenButton);
        return new MenuDashboardPage(driver);
    }

}
