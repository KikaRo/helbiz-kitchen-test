package helbizkitchen.page_objects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.awt.*;

public class MenuDashboardPage extends BasePage {

    @FindBy(xpath = "//div[@class='hero__content']")
    private WebElement pageIdentifier;

    @FindBy(xpath = "//img[@alt='pizza']")
    private WebElement pizzaMenu;

    @FindBy(xpath = "//h2[@class='individual-dish-card__content__name hidden-to-l']")
    private WebElement menuItemPopup;

    @FindBy(xpath = "//button[@class='button button--standard add-order-bar__add-button']")
    private WebElement addToOrderButton;

    public MenuDashboardPage(WebDriver driver) { super(driver); }

    public boolean isActive() {
        return isElementVisible(pageIdentifier) && pageIdentifier.getText().contains("Welcome to Helbiz Kitchen");
    }

    public void clickOnPizzaMenu() { clickOnElement(pizzaMenu); }

    public boolean isMenuItemPopupActive() { return isElementVisible(menuItemPopup); }

    public void clickOnAddToOrderButton() { clickOnElement(addToOrderButton); }

    public LoginPage clickOnAddToOrderButtonWithoutLogingIn() {
        clickOnElement(addToOrderButton);
        return new LoginPage(driver);
    }

}
