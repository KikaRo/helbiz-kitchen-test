package helbizkitchen.page_objects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

public class SignUpPage extends BasePage {

    @FindBy(xpath = "//div[@class='register']")
    private WebElement pageIdentifier;

    @FindBy(xpath = "//input[@name='email']")
    private WebElement emailInput;

    @FindBy(xpath = "//input[@name='password']")
    private WebElement passwordInput;

    @FindBy(xpath = "//input[@name='confirmPassword']")
    private WebElement confirmPasswordInput;

    @FindBy(xpath = "//button[@value='Next']")
    private WebElement nextButton;

    @FindBy(xpath = "(//span[@class='toggle-password-visibility-wrapper'])[1]")
    private WebElement visibilityButtonPassword;

    @FindBy(xpath = "(//span[@class='toggle-password-visibility-wrapper'])[2]")
    private WebElement visibilityButtonConfirmPassword;

    public SignUpPage(WebDriver driver) { super(driver); }

    public boolean isActive() { return isElementVisible(pageIdentifier) && pageIdentifier.getText().contains("Create your account"); }

    public void fillEmailInput(String email) {
        emailInput.clear();
        fillInInputField(emailInput,email);
    }

    public void fillPasswordInput(String password) {
        passwordInput.clear();
        fillInInputField(passwordInput, password);
    }

    public void fillConfirmPasswordInput(String confirmPassword) {
        confirmPasswordInput.clear();
        fillInInputField(confirmPasswordInput, confirmPassword);
    }

    public boolean isNextButtonClickable() { return isElementClickable(nextButton); }

    public void clickOnVisibilityButtonPassword() {
        moveMouseToVisibilityButton();
        clickOnElement(visibilityButtonPassword);
    }

    public boolean isPasswordEncrypted() { return passwordInput.getAttribute("type").equals("password"); }

    public boolean isOkoVisible() { return isElementVisible(visibilityButtonPassword); }

    public void moveMouseToVisibilityButton() {
        Actions actions = new Actions(driver);
        actions.moveToElement(visibilityButtonPassword).perform();
    }

    public boolean isEnteredPasswordVisible(String enteredPassword) {
        return isElementVisible(findFirstElementThatContainsText(enteredPassword));
    }
}