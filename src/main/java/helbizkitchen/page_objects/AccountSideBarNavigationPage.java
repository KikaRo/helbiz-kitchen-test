package helbizkitchen.page_objects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AccountSideBarNavigationPage extends BasePage {

    public AccountSideBarNavigationPage(WebDriver driver) { super(driver); }

    public PersonalDetailsPage openPersonalDetailsPage() {
        clickOnElement(findFirstElementThatContainsText("Personal Details"));
        return new PersonalDetailsPage(driver);
    }
}
