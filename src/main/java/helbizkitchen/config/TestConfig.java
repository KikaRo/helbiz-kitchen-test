package helbizkitchen.config;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import finplan.enums.BrowserType;

public class TestConfig {
    private Properties properties = new Properties();

    private BrowserType browserType;
    private String firefoxDriverPath;
    private String chromeDriverPath;
    private String edgeDriverPath;

    private String url;
    private String email;
    private String password;

    private String screenshotLocation;

    private static final String configFileName = "src/main/resources/config.properties";

    public TestConfig(){
        InputStream inputStream;
        try{
            inputStream = new FileInputStream(configFileName);
            this.properties.load(inputStream);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        this.browserType = setBrowserType(properties.getProperty("browserType"));
        this.firefoxDriverPath = properties.getProperty("firefoxDriverPath");
        this.chromeDriverPath = properties.getProperty("chromeDriverPath");
        this.edgeDriverPath = properties.getProperty("edgeDriverPath");

        this.url = properties.getProperty("url");
        this.email = properties.getProperty("email");
        this.password = properties.getProperty("password");

        this.screenshotLocation = properties.getProperty("screenshotLocation");
    }

    private BrowserType setBrowserType(String browserName){
        for (BrowserType browser : BrowserType.values()){
            if (browser.toString().equalsIgnoreCase(browserName)) {
                return browser;
            }
        }
        return null;
    }

    public Properties getProperties() { return properties; }

    public BrowserType getBrowserType() { return browserType; }

    public String getFirefoxDriverPath() { return firefoxDriverPath; }

    public String getChromeDriverPath() { return chromeDriverPath; }

    public String getEdgeDriverPath() { return edgeDriverPath; }

    public String getUrl() { return url; }

    public void setUrl(String url) { this.url = url; }

    public String getEmail() { return email; }

    public void setUsername(String username) { this.email = username; }

    public String getPassword() { return password; }

    public void setPassword(String password) { this.password = password; }

    public String getScreenshotLocation() { return screenshotLocation; }

}
