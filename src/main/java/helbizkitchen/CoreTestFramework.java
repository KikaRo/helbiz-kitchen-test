package helbizkitchen;

import helbizkitchen.page_objects.DashboardPage;
import helbizkitchen.page_objects.LoginPage;
import helbizkitchen.page_objects.SignUpPage;
import org.openqa.selenium.*;
import helbizkitchen.webdriver.BrowserDriver;

public class CoreTestFramework {
    public static void main(String[] args) throws InterruptedException {
        WebDriver driver = BrowserDriver.getDriver();
        DashboardPage dashboardPage = new DashboardPage(driver);
        System.out.println(dashboardPage.isActive());
        System.out.println(dashboardPage.isGetStartedButtonVisible());
        LoginPage loginPage = dashboardPage.clickOnGetStartedButton();
        System.out.println(loginPage.isActive());
        SignUpPage signUpPage = loginPage.clickOnSignUpButton();
        signUpPage.clickOnVisibilityButtonPassword();
        signUpPage.fillPasswordInput("password1");
        signUpPage.wait(20);
        System.out.println(signUpPage.isElementVisible(signUpPage.findFirstElementThatContainsText("password1")));

    }
}
