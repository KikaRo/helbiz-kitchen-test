package helbizkitchen.utils;

public class TimeUtil {

    public static final int ELEMENT_VISIBLE_TIME = 10;
    public static final int ELEMENT_DISAPPEAR_TIME = 1;
    public static final int EDIT_PAGE_LOAD_TIME = 10;

}
