package helbizkitchen.utils;

import helbizkitchen.config.TestConfig;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class ScreenshotUtil {

    public static void makeScreenshot(WebDriver driver, String imageName) {
        TestConfig config = new TestConfig();
        String location = config.getScreenshotLocation();

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat formater = new SimpleDateFormat("yyyyMMdd-hh-mm-ss");

        try {
            File captureFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            File screenshot = null;
            if(StringUtils.isBlank(imageName)){
                screenshot = new File(location + "/screenshot-" + formater.format(calendar.getTime()) + ".png");
            } else {
                screenshot = new File(location + "/" + imageName + "-" + formater.format(calendar.getTime()) + ".png");
            }
            FileUtils.copyFile(captureFile, screenshot);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
