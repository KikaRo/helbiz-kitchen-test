package helbizkitchen.webdriver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.UnreachableBrowserException;
import java.util.concurrent.TimeUnit;

import helbizkitchen.config.TestConfig;

public class BrowserDriver {
    private static WebDriver driver;

    public static WebDriver getDriver() {
        if (driver == null) {
            try {
                driver = helbizkitchen.webdriver.WebDriverFactory.initDriver(new TestConfig());
                driver.manage().window().maximize();
                driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return driver;
    }

    public static WebDriver getDriver(TestConfig config) {
        if (driver != null) {
            close();
        }
        try {
            driver = helbizkitchen.webdriver.WebDriverFactory.initDriver(config);
            driver.manage().window().maximize();
            driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return driver;
    }

    public static void close() {
        try {
            getDriver().quit();
            driver = null;
        } catch (UnreachableBrowserException e) {
            e.printStackTrace();
        }
    }
}
