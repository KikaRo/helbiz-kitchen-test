package helbizkitchen.webdriver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;


import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import helbizkitchen.config.TestConfig;

import java.net.MalformedURLException;
import java.net.URL;

public class WebDriverFactory {
    public static WebDriver initDriver(TestConfig config) throws MalformedURLException {
        switch (config.getBrowserType()) {
            case FIREFOX: {
                System.setProperty("webdriver.gecko.driver", config.getFirefoxDriverPath());
                return new FirefoxDriver();
            }
            case CHROME: {
                System.setProperty("webdriver.chrome.driver", config.getChromeDriverPath());
                return new ChromeDriver();
            }
            case CHROME_HEADLESS: {
                System.setProperty("webdriver.chrome.driver", config.getChromeDriverPath());
                ChromeOptions options = new ChromeOptions();
                options.addArguments("headless");
                options.addArguments("--disable-gpu");
                options.addArguments("disable-infobars");
                options.addArguments("--disable-extensions");
                options.addArguments("window-size=1920x1080");
                options.addArguments("--no-sandbox");
                return new ChromeDriver(options);
            }
            case CHROME_DOCKER: {
                ChromeOptions options = new ChromeOptions();
                options.addArguments("headless");
                options.addArguments("--disable-gpu");
                options.addArguments("disable-infobars");
                options.addArguments("--disable-extensions");
                options.addArguments("window-size=1920x1080");
                options.addArguments("--no-sandbox");
                return new RemoteWebDriver(new URL("http://selenium:4444/wd/hub"), options);
            }
            case EDGE: {
                System.setProperty("webdriver.edge.driver", config.getEdgeDriverPath());
                return new EdgeDriver();
            }
            default: {
                System.setProperty("webdriver.chrome.driver", config.getChromeDriverPath());
                return new ChromeDriver();
            }
        }
    }
}
